import com.sun.source.tree.Tree;

public class LowestCommonAncestor {
    public TreeNode lcabst(TreeNode root, TreeNode node1, TreeNode node2)
    {
        if(root == null)
        {
            return null;
        }
        if(root.data < Math.min(node1.data, node2.data))
        {
            return lcabst(root.right ,node1, node2);
        }
        else if(root.data > Math.max(node1.data, node2.data))
        {
            return lcabst(root.left, node1, node2);
        }
        else
        {
            return root;
        }
    }
    public TreeNode lcabt(TreeNode root, TreeNode node1, TreeNode node2)
    {
        if(root == null)
        {
            return null;
        }
        TreeNode left = lcabt(root.left, node1, node2);
        TreeNode right = lcabt(root.right, node1, node2);
        if(left!=null && right!=null)
        {
            return root;
        }
        if(left == null && right == null)
        {
            return null;
        }
        return left!=null?left:right;
    }
}
