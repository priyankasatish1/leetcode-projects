import java.util.Arrays;
import java.util.Iterator;

public class Codec {
    public String Serialize(TreeNode treeNode)
    {
        StringBuilder sb = new StringBuilder();
        serializeHelper(treeNode,sb);
        return sb.toString();

    }
    public void serializeHelper(TreeNode node, StringBuilder sb)
    {
        if(node == null)
        {
            sb.append("null"+",");
            return;
        }
        sb.append(node.value+",");
        serializeHelper(node.left,sb);
        serializeHelper(node.right,sb);
    }
    public TreeNode deserialize(String data)
    {
        String[] dataArray = data.split(",");
        Iterator<String> it = Arrays.stream(dataArray).iterator();
        return deserializeHelper(it);
    }
    public TreeNode deserializeHelper(Iterator<String> it)
    {
        if(!it.hasNext())
        {
            return null;
        }
        String val = it.next();
        if(val.equals("null"))
        {
            return null;
        }
        TreeNode root = new TreeNode(Integer.parseInt(val));
        root.left = deserializeHelper(it);
        root.right = deserializeHelper(it);
        return root;
    }
}
