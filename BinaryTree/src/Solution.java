public class Solution {
    public static void main(String[] args)
    {
        BinarySearchTree bst = new BinarySearchTree();
        TreeNode node = bst.insert(50);
        bst.insert(30);
        bst.insert(20);
        bst.insert(40);
        bst.insert(70);
        bst.insert(60);
        bst.insert(80);
        Codec cc = new Codec();
        String ans = cc.Serialize(node);
        TreeNode ans1 = cc.deserialize("50,30,20,null,null,40,null,null,70,60,null,null,80,null,null");
    }
}
