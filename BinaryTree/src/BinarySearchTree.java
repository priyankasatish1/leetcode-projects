
public class BinarySearchTree {
    TreeNode root;

    public BinarySearchTree()
    {
        root = null;
    }

    public TreeNode insert(int key) {
        root = insertNode(key, root);
        return root;
    }

    public TreeNode insertNode(int key, TreeNode root)
    {
        if (root == null)
        {
            root = new TreeNode(key);
            return root;
        }
        if (key < root.value)
        {
            root.left = insertNode(key, root.left);
        }
        else if (key > root.value)
        {
            root.right = insertNode(key, root.right);
        }
        return root;
    }
}
