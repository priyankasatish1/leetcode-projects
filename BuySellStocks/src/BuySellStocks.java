public class BuySellStocks {
    public static int maxProfit(int[] prices)
    {
        int maxProfit = 0;
        int minPrice = Integer.MAX_VALUE;
        for(int index = 0; index < prices.length; index++)
        {
            if(prices[index] < minPrice)
            {
                minPrice = prices[index];
            }
            else if(prices[index]- minPrice > maxProfit)
            {
                maxProfit = prices[index] - minPrice;
            }
        }
        return maxProfit;
    }
    public static void main(String [] args)
    {
        int[] prices = {7,1,5,3,6,4};
        int result = maxProfit(prices);
        System.out.println(result);
    }
}
