import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class MergeIntervals {
    public int[][] merge(int[][] intervals)
    {
        int[] previousElement = null;
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        ArrayList<int[]> intervalList = new ArrayList<>();
        for(int[] element : intervals)
        {
            if(previousElement == null || element[0] > previousElement[1]){
                intervalList.add(element);
                previousElement = element;
            }
            else if(element[1] > previousElement[1])
            {
                previousElement[1] = element[1];

            }
        }
        int[][] newIntervals = intervalList.toArray(new int[intervalList.size()][2]);
        return newIntervals;
    }
}
