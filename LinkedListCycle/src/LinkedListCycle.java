import java.util.HashSet;
import java.util.Set;

public class LinkedListCycle
{
    public boolean hasCycle(ListNode head)
    {
        Set<ListNode> map = new HashSet<>();
        while (head!=null)
        {
            if(map.contains(head))
            {
                return true;
            }
            head = head.next;
        }
        return false;
    }
    public boolean isCycle(ListNode head)
    {
        ListNode fast = head;
        ListNode slow = head;
        if(head == null)
        {
            return false;
        }
        while(slow.next!=null && fast.next != null)
        {
            slow = slow.next;
            fast = fast.next.next;
            if(fast == null)
            {
                return false;
            }
            if(fast == slow)
            {
                return true;
            }
        }
        return false;
    }
}
