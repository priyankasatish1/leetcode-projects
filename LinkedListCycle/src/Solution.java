public class Solution {
    public static void main(String[] args)
    {
        LinkedListCycle llc = new LinkedListCycle();
        LinkedListUtil ll = new LinkedListUtil();
        ListNode head = ll.insertAtTail(10);
        ll.insertAtTail(20);
        ll.insertAtTail(30);
        ll.insertAtTail(40);
        ll.insertAtTail(50);
        ll.insertAtTail(60).next = head;
        llc.isCycle(head);
    }
}
