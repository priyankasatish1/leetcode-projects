public class LinkedListUtil {
    ListNode head;
    public LinkedListUtil()
    {
        head = null;
    }
    public ListNode insertAtTail(int data)
    {
        ListNode newNode = new ListNode(data);
        if(head == null)
        {
            head = newNode;
        }
        else
        {
            ListNode temp = head;
            while (temp.next!=null)
            {
                temp = temp.next;
            }
            temp.next = newNode;
        }
        return head;
    }
}
