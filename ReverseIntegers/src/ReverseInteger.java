public class ReverseInteger {
    public static int reverse(int number)
    {
        int newNumber = number;
        int digit;
        long reversed=0;
        int max = Integer.MAX_VALUE;
        int min = Integer.MIN_VALUE;
        while(newNumber != 0)
        {
            digit = newNumber % 10;
            reversed = reversed*10 + digit;
            newNumber = newNumber/10;
        }
        if(reversed > max || reversed < min)
        {
            return 0;
        }
        return (int)reversed;
    }

    public static void main(String[] args)
    {
        int result = reverse(12345);
        System.out.println(result);
    }
}
