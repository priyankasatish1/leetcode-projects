import java.util.Arrays;

public class ValidTree {
    public static boolean validTree(int n, int[][] edges) {
        int[] visited = new int[n];
        Arrays.fill(visited,-1);
        for(int[]edge :edges )
        {
            int p1 =findParent(edge[0],visited);
            int p2 =findParent(edge[1], visited);
            if(p1==p2)
            {
                return false;
            }
            visited[p1] = p2;
        }
        return edges.length==n-1;

    }
    public static int findParent(int vertex, int[] visited)
    {
        if(visited[vertex]!=-1) {
            return vertex;
        }
        return findParent(visited[vertex],visited);
    }


    public static void main(String [] args)
    {
        int[][] edges = {{0,1},{0,2},{0,3},{1,4}};
        //int[][] edges  = {{0,1}, {1,2}, {2,3}, {1,3}, {1,4}};
        boolean result = validTree(5,edges);
    }
}
