public class CustomHash {
    int CAPACITY = 5;
    ListNode[] map;
    public CustomHash()
    {
        map = new ListNode[CAPACITY];
    }
    public ListNode findElement(ListNode node, int key)
    {
        while(node!=null)
        {
            if(node.key == key)
            {
                return node;
            }
            node = node.next;
        }
        return null;
    }
    public ListNode insertNode(ListNode node, ListNode[] map, int hash)
    {
        ListNode head = map[hash];
        if(head == null)
        {
            head = node;
        }
        else
            {
            ListNode temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
        return head;
    }
    public void put(int key, int value)
    {
        int hash = key % CAPACITY;
        ListNode foundNode = findElement(map[hash], key);
        if(foundNode!=null)
        {
            foundNode.value = value;
        }
        else
        {
            map[hash] = insertNode(new ListNode(key,value), map, hash);
        }
    }
    public int get(int key)
    {
        int hash = key % CAPACITY;
        ListNode foundNode = findElement(map[hash],key);
        if(foundNode!=null)
        {
            return foundNode.value;
        }
        else
        {
            return -1;
        }
    }
    public void remove(int key)
    {
        int hash = key % CAPACITY;
        ListNode head = map[hash];
        removeKey(head, key, map, hash);

    }
    public ListNode removeKey(ListNode head , int key, ListNode[] map, int hash)
    {   ListNode curr = head;
        ListNode prev = null;
        if(head == null)
        {
            return null;
        }
        if(curr.key == key)
        {
            curr = curr.next;
        }
        head = curr;
        while(curr!=null)
        {
            if(curr.key != key)
            {
                prev = curr;
                curr = curr.next;
            }
            else
            {
                prev.next = curr.next;
                curr = curr.next;
            }
        }
        map[hash] = head;
        return head;
    }
}
