public class ListNode {
    ListNode next;
    int key;
    int value;
    public ListNode(int _key, int _value)
    {
        key = _key;
        value = _value;
        next = null;
    }
    public ListNode()
    {

    }
}
