import java.util.Stack;
public class QStack {
    Stack<Integer> stack;
    Stack<Integer> buffer;
    public QStack()
    {
        stack = new Stack<>();
        buffer = new Stack<>();
    }
    public void push(int number)
    {
        stack.push(number);
    }
    public int pop()
    {
        Integer front;
        if(!buffer.empty())
        {
             front = buffer.pop();
        }
        else
        {
            while(!stack.empty())
            {
                buffer.push(stack.pop());
            }
            front = buffer.pop();
        }
        return front;
    }
    public int peek()
    {
        Integer front;
        if(!buffer.empty())
        {
            front = buffer.peek();
        }
        else
        {
            while(!stack.empty())
            {
                buffer.push(stack.pop());
            }
            front = buffer.peek();
        }
        return front;
    }

    public boolean empty()
    {
        if(buffer.empty() && stack.empty())
        {
            return true;
        }
        return false;
    }
}
