public class Solution {
    public static void main(String [] args)
    {
        QStack qs = new QStack();
        qs.push(5);
        qs.push(4);
        qs.push(3);
        int ans = qs.pop();
        System.out.println(ans);
        qs.push(2);
        qs.push(1);
        int ans2 = qs.pop();
        System.out.println(ans2);
        int ans3 = qs.pop();
        int ans4 = qs.pop();
        int ans5 = qs.pop();
        boolean val = qs.empty();
    }
}
