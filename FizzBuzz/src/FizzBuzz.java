import java.util.ArrayList;
import java.util.List;


public class FizzBuzz {
    public static List<String> fizzBuzz(int number) {
        List<String> numList = new ArrayList<>();
        for(int i = 1; i <= number; i++)
        {
            if(i % 15 == 0)
            {
                numList.add("FizzBuzz");
            }
            else if(i % 3 == 0)
            {
                numList.add("Fizz");
            }
            else if(i % 5 == 0)
            {
                numList.add("Buzz");
            }
            else
            {
                numList.add(Integer.toString(i));
            }
        }
        return numList;

    }
    public static void main(String [] args)
    {
        List<String> ans = fizzBuzz(15);
        for(String element : ans)
        {
            System.out.println(element);
        }

    }
}
