import java.util.Stack;

public class MaxStack {
    Stack<Integer> stack;
    Stack<Pair> maxStack;
    public MaxStack()
    {
        stack = new Stack<>();
        maxStack = new Stack<>();
    }

    public void push(int number)
    {
        stack.push(number);
        if(stack.empty())
        {
            maxStack.push(new Pair(number,1));
        }
        else if(stack.peek() == maxStack.peek().keys)
        {
            int newFrequency = maxStack.peek().values +1;
            maxStack.pop();
            maxStack.push(new Pair(number,newFrequency));
        }
        else if(stack.peek() > maxStack.peek().keys)
        {
            maxStack.push(new Pair(number,1));
        }
    }

    public int pop()
    {
        int number = stack.pop();
        if(maxStack.peek().keys == number)
        {
            if(maxStack.peek().values == 0)
            {
                maxStack.pop();
            }
            else if(maxStack.peek().values > 0)
            {
               int newFrequency = maxStack.peek().values -1;
               maxStack.pop();
               maxStack.push(new Pair(number,newFrequency));
            }
        }
        return number;

    }

    public int top()
    {
        int stackTop = stack.peek();
        return stackTop;
    }
    public int peekMax()
    {
        int maxTop = maxStack.peek().keys;
        return maxTop;
    }
    public int popMax()
    {
        int number = maxStack.peek().keys;
        Stack<Integer> buffer = new Stack<>();
        while(stack.peek() != number)
        {
            buffer.push(stack.pop());
        }
        pop();
        while(!buffer.empty())
        {
            push(buffer.pop());
        }
        return number;
    }
}
