import java.util.*;

import static java.util.Arrays.*;

public class FunctionTime {
 public static int[] exclusiveTime(int n, List<String> logs) {
        Stack<LogString> logStack = new Stack<>();
        LogString log;
        int[] res = new int[n];
        int prev = 0;
        for( String logString: logs)
        {
            log = new LogString(logString);
            if(log.type.equals("start"))
            {
                if (!logStack.isEmpty())
                {
                    res[logStack.peek().processName] += log.timeStamp - prev;
                }
                logStack.push(log);
                prev = log.timeStamp;
            }
            else
            {
                res[log.processName] += log.timeStamp - prev + 1;
                prev = log.timeStamp + 1;
                logStack.pop();

            }
        }
        return res;
    }

 public static int[] getExclusiveTime(int n, List<String> logs)
 {
     Stack<LogString> logStack = new Stack<>();
     int[] exclusiveTimes = new int[n];
     int previousMilestoneTime = 0;
     for(String log : logs)
     {
         LogString logObject = new LogString(log);
         if(logObject.type.equals("start"))
         {
             if(!logStack.isEmpty())
             {
                 exclusiveTimes[logStack.peek().processName] += logObject.timeStamp - previousMilestoneTime;
             }
             logStack.push(logObject);
             previousMilestoneTime = logObject.timeStamp;
         }
         else
         {
             exclusiveTimes[logObject.processName] += logObject.timeStamp - previousMilestoneTime;
             previousMilestoneTime = logObject.timeStamp+1;
             logStack.pop();
         }
     }
     return exclusiveTimes;
 }
    public static void main(String[] args) {
        List<String> ip = asList(new String[]{"0:start:0","0:start:2","0:end:5","0:start:6","0:end:6","0:end:7"});
        System.out.println(getExclusiveTime(3, ip).toString());
    }
}
