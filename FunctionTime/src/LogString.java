public class LogString {
    Integer processName;
    String type;
    Integer timeStamp;

    public LogString(String logString)
    {
        String[] logObjects = logString.split(":");
        processName = Integer.parseInt(logObjects[0]);
        type = logObjects[1];
        timeStamp = Integer.parseInt(logObjects[2]);
    }
}
