import java.util.HashMap;
import java.util.Map;

public class LRUCache1
{
    int cacheSize;
    Map<Integer,ListNode> map;
    DoublyLinkedList doublyLinkedList = new DoublyLinkedList();
    public LRUCache1(int capacity)
    {
         cacheSize = capacity;
         map = new HashMap<>();
    }
    public int get(int address)
    {
        if(!map.containsKey(address))
        {
            return -1;
        }
        else
        {
            ListNode node = map.get(address);
            doublyLinkedList.deleteNode(node);
            map.remove(address);

            ListNode newEntry = new ListNode(address,node.data);
            map.put(address,newEntry);
            doublyLinkedList.addToHead(newEntry);
            return newEntry.data;
        }
    }
    public void put(int address, int value)
    {
        if(map.containsKey(address))
        {
            ListNode node = map.get(address);
            map.remove(address);
            doublyLinkedList.deleteNode(node);

            map.put(address,new ListNode(address,value));
            doublyLinkedList.addToHead(map.get(address));
        }
        else
        {
            if(map.size()==cacheSize)
            {

                int delAddress = doublyLinkedList.deleteAtTail();
                map.remove(delAddress);

                map.put(address,new ListNode(address,value));
                doublyLinkedList.addToHead(map.get(address));
            }
            else
            {
                map.put(address,new ListNode(address,value));
                doublyLinkedList.addToHead(map.get(address));
            }
        }
    }
    public void printCache() {
        for (Map.Entry<Integer, ListNode> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue().data);
        }
        System.out.println("\n");
    }
}
