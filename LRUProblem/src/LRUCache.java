import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LRUCache {
    int cacheSize;
    Map< Integer, ListNode> map;
    LinkedListUtil ln = new LinkedListUtil();
    public LRUCache(int capacity)
    {
        cacheSize = capacity;
        map = new HashMap<>();
    }
    public int get(int address)
    {
        if(map.containsKey(address))
        {
            ListNode node = map.get(address);
            int entryValue = node.data;
            ln.deleteNode(node);
            ListNode newNodeEntry = new ListNode(address,entryValue);
            ln.addToHead(newNodeEntry);
            map.remove(address);
            map.put(address,newNodeEntry);
            return newNodeEntry.data;
        }
        else
        {
            return -1;
        }

    }

    public void printCache() {
        for (Map.Entry<Integer, ListNode> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue().data);
        }
        System.out.println("\n");
    }

    public void put(int address, int data)
    {
        if(map.containsKey(address))
        {
            ln.deleteNode(map.get(address));
            map.remove(address);
            map.put(address,new ListNode(address, data));
            ln.addToHead(map.get(address));
        }
        else
        {
            if(map.size() >= cacheSize)
            {
                int delAddress = ln.deleteAtTail();
                map.remove(delAddress);

                map.put(address,new ListNode(address, data));
                ln.addToHead(map.get(address));
            }
            else
            {
                map.put(address,new ListNode(address, data));
                ln.addToHead(map.get(address));
            }

        }

    }

}