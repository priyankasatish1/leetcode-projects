public class LinkedListUtil {
    public ListNode head;
    public ListNode tail;

    public LinkedListUtil() {
        head = null;
        tail = null;
    }

    public ListNode addToHead(ListNode node) {
        if(head == null)
        {
            head = node;
            tail = node;
        }
        else
        {
            node.next = head;
            head.previous = node;
            head = node;
        }
        return head;
    }

    public int deleteAtTail() {
        ListNode temp = tail;
        ListNode prevNode = tail.previous;
        if(prevNode == null)
        {
            head = null;
            tail = null;
            return temp.addr;
        }
        tail.previous = null;
        prevNode.next = null;
        tail = prevNode;
        return temp.addr;
    }

    public void deleteNode(ListNode node) {
        if (node.previous != null) {
            node.previous.next = node.next;
        } else {
            head = node.next;
        }
        if (node.next != null) {
            node.next.previous = node.previous;
        } else {
            tail = node.previous;
        }
    }
}
