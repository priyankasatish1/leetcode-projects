public class DoublyLinkedList {
    ListNode head;
    ListNode tail;
    public DoublyLinkedList()
    {
        head = new ListNode(-1,-1);
        tail = new ListNode(-1,-1);
        head.next = tail;
        tail.previous = head;
    }
    public void addToHead(ListNode node)
    {
        node.next = head.next;
        head.next.previous = node;
        head.next = node;
        node.previous = head;
    }
    public void deleteNode(ListNode node)
    {
        node.previous.next = node.next;
        node.next.previous = node.previous;
    }
    public int deleteAtTail()
    {
        ListNode node = tail.previous;
        node.previous.next = tail;
        tail.previous= node.previous;
        return node.addr;
    }
}
