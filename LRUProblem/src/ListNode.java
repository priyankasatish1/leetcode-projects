public class ListNode {
    ListNode previous;
    ListNode next;
    int addr;
    int data;

    public ListNode(int address, int value)
    {
        addr = address;
        previous = null;
        data = value;
        next = null;
    }
    public ListNode()
    {

    }

}
