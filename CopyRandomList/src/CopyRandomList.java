import java.util.HashMap;
import java.util.Map;

public class CopyRandomList {
    public RandomListNode deepCopy(RandomListNode head)
    {
        Map<RandomListNode,RandomListNode> map = new HashMap<>();
        if(head == null)
        {
            return null;
        }
        RandomListNode temp = head;
        while(temp!=null)
        {
            map.put(temp,new RandomListNode(temp.label));
            temp = temp.next;
        }
        RandomListNode temp2 = head;
        while (temp2!=null)
        {
            RandomListNode copyNode = map.get(temp2);
            copyNode.next = map.get(temp2.next);
            copyNode.randomNext = map.get(temp2.randomNext);
            temp2 = temp2.next;
        }
        return map.get(head);
    }
}
