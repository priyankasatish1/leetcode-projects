public class RandomListNode {
    int label;
    RandomListNode next;
    RandomListNode randomNext;
    public RandomListNode(int data)
    {
        label = data;
        next = null;
        randomNext = null;
    }
}
