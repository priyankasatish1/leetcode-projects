import java.util.Stack;

public class ReversePolishNotation {
    public static int evaluateNotation(String[] tokens)
    {
        int finalResult = 0;
        Stack<Integer> numStack = new Stack<>();
        for(String token : tokens)
        {
            if(isInteger(token))
            {
                numStack.push(Integer.parseInt(token));
            }
            else
            {
                int operand1 =0;
                int operand2=0;
                if(!numStack.empty())
                {
                     operand1 = numStack.pop();
                     operand2 = numStack.pop();
                }
                finalResult = calculate(operand1,operand2,token);
                numStack.push(finalResult);
            }
            
        }
        return numStack.pop();

    }
    public static boolean isInteger(String literal)
    {
        try
        {
            int number = Integer.parseInt(literal);
        }
        catch (NumberFormatException ex)
        {
            return false;
        }
        return true;
    }
    public static int calculate(int operand1, int operand2, String operator)
    {
        int result = 0;
        switch (operator)
        {
            case "+":
                result =  operand1 + operand2;
                break;

            case "-":
               result = operand2 - operand1;
                break;

            case "*":
                result= operand1*operand2;
                break;
            case "/":
                result = operand2/operand1;
                break;

            default:
                break;

        }
        return result;
    }
    public static void main(String[] args)
    {
        String[] input = {"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"};
        int ans = evaluateNotation(input);
        System.out.println(ans);
    }

}
