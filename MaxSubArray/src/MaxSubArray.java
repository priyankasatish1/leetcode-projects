public class MaxSubArray {
    public static int maxSubArray(int[] input)
    {
        int sum = input[0];
        int result = input[0];
        for(int index = 1; index < input.length; index++)
        {
            if(sum < 0)
            {
                sum = input[index];
            }
            else
            {
                sum+= input[index];
            }
            result = Math.max(sum,result);
        }
        return result;
    }
    public static void main(String[] args)
    {
        int[] input = {-2,1,-3,4,-1,2,1,-5,4};
        int res = maxSubArray(input);
        System.out.println(res);
    }
}
