import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

public class MeetingRoomsII {
    public int minMeetingRoomsII(int[][] intervals) {
        if(intervals == null || intervals.length == 0)
            return 0;
        Arrays.sort(intervals, (a,b) -> a[0] - b[0]);
        PriorityQueue<int[]> minHeap = new PriorityQueue<>((a,b)-> a[1]-b[1]);
        minHeap.add(intervals[0]);
        for(int index = 1; index < intervals.length; index++)
        {
            int[] currentMeeting = intervals[index];
            int[] earlierMeeting = minHeap.remove();
            if(currentMeeting[0]>=earlierMeeting[1])
            {
                earlierMeeting[1] = currentMeeting[1];
            }
            else
            {
                minHeap.add(currentMeeting);
            }
            minHeap.add(earlierMeeting);
        }
        return minHeap.size();
    }
    public int minMeetingRooms(int[][] intervals){
        if(intervals == null || intervals.length == 0)
            return 0;
        int[] previousInterval = null;
        ArrayList<int[]> intervalList = new ArrayList<>();
        Arrays.sort(intervals, (a,b) -> a[0] - b[0]);
        for (int[] interval : intervals)
        {
            if(previousInterval == null || interval[0] < previousInterval[1])
            {
                previousInterval = interval;
                intervalList.add(previousInterval);
            }
            else if(interval[0]>=previousInterval[1])
            {
                previousInterval[1] = interval[1];
            }
        }
        return intervalList.size();
    }

}
