import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class SerializeDeserialize {
    public TreeNode deserialize(String data)
    {
        if (data.isEmpty()) return null;
        String[] array = data.split(",");
        Queue<Integer> queue = new LinkedList<>();
        for(String element: array)
        {
            queue.offer(Integer.parseInt(element));
        }
        return deserializeHelper(queue, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
    public TreeNode deserializeHelper(Queue<Integer> queue, int lo, int hi)
    {
        if(queue.isEmpty())
        {
            return null;
        }
        int val = queue.peek();
        if(val < lo||val > hi)
        {
            return null;
        }
        TreeNode root = new TreeNode(val);
        queue.poll();
        root.left = deserializeHelper(queue, lo, val);
        root.right = deserializeHelper(queue, val, hi);
        return root;
    }

    public String serialize(TreeNode root)
    {
        StringBuilder sb = new StringBuilder();
        serializeHelper(root, sb);
        return sb.toString();
    }
    public void serializeHelper(TreeNode root, StringBuilder sb)
    {
        if(root == null)
        {
            return;
        }
        sb.append(root.data+",");
        serializeHelper(root.left, sb);
        serializeHelper(root.right, sb);
    }
     /* public TreeNode deserializeHelper(String[] array, int lo, int hi)
    {
        if(lo > hi)
        {
            return null;
        }
        if(array[lo]=="")
        {
            return null;
        }

        TreeNode root = new TreeNode(Integer.parseInt(array[lo]));
        int divIndex = getDivIndex(array,lo+1,hi,root.data);
        root.left = deserializeHelper(array,lo+1,divIndex-1);
        root.right = deserializeHelper(array,divIndex, hi);
        return root;
    }
    public int getDivIndex(String[] array, int lo, int hi, int val) {
        int index;
        for ( index = lo; index < hi; index++) {
            if (val < Integer.parseInt(array[index])) {
                break;
            }
        }
        return  index;
    }*/
}
