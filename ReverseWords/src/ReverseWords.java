public class ReverseWords {
    public static String reverseWords(String input)
    {
        String[] inputArray = input.split(" ");
        int i =0;
        int j = inputArray.length-1;
        StringBuilder sb = new StringBuilder();
        while(i<j)
        {
            String temp = inputArray[i];
            inputArray[i]=inputArray[j];
            inputArray[j] = temp;
            i++;
            j--;
        }
        for(String element : inputArray)
        {
            if(!element.equals("")) {
                sb.append(element + " ");
            }
        }
       String rs =  sb.toString();
        rs = rs.trim();

        boolean isEmptyLast = rs.endsWith(" ");
        if(isEmptyLast)
        {
            rs = rs.substring(0,rs.length()-1);
        }
        return rs;
    }
    public static void main(String[] args)
    {
        String input = "a good   example";
        String res = reverseWords(input);
    }
}
