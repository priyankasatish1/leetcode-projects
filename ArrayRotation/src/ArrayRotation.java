
public class ArrayRotation {
    public int searchInArray(int[] array, int target)
    {
        int lo = 0;
        int hi = array.length-1;
        int ans = search(lo, hi, array, target);
        return ans;
    }
    public int search(int lo, int hi, int[] array, int target)
    {
        if(lo > hi)
        {
            return -1;
        }
        if(lo == hi)
        {
            if(target == array[lo])
            {
                return lo;
            }
            return -1;
        }
        int mid = (lo + hi) /2;
        if(target == array[lo])
        {
            return lo;
        }
        if(target == array[hi])
        {
            return hi;
        }
        if(array[mid]==target)
        {
            return mid;
        }
        if(isInBetween(array,target,lo,mid))
        {
            return search(lo, mid, array, target);
        }
        else if(isInBetween(array,target,mid+1, hi))
        {
            return search(mid+1, hi, array, target);
        }
        else
        {
            if(array[lo] > array[mid])
            {
                return search(lo,mid,array,target);
            }
            else if(array[hi] < array[mid+1])
            {
                return search(mid+1,hi, array, target);
            }
        }
        return -1;
    }
    public boolean isInBetween(int[] array, int target, int lo, int hi)
    {
        if(array[lo] <= target && array[hi] >= target)
        {
            return true;
        }
        return false;
    }
}
