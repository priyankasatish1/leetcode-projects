public class LinkedList {
    ListNode head;

    public LinkedList() {
        head = null;

    }

    public ListNode insertToEnd(int data) {
        ListNode newNode = new ListNode(data);
        if (head == null) {
            head = newNode;
            //return head;
        } else {
            ListNode temp = head;
            while (temp.link != null) {
                temp = temp.link;
            }
            temp.link = newNode;
        }
        return head;
    }
}
