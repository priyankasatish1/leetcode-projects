import java.util.List;

public class ReverseLinkedList {
    public ListNode reverse(ListNode head)
    {
        ListNode previous = null, next;
        ListNode current = head;
        while(current != null)
        {
            next = current.link;// Save the current element's link
            current.link = previous; //make the current's link point to previous node
            previous = current; //make the previous element the current element
            current = next;
        }
        return previous;
    }
}
