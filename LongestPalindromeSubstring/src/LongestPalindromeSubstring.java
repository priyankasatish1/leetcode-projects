public class LongestPalindromeSubstring {
    public String longestPalindrome(String inputString)
    {
        String maxSubString = "";
        if(inputString == null || inputString.length() < 2)
        {
            return inputString;
        }
        for(int index = 0; index < inputString.length();index++)
        {
            String substring1 = extendAroundCenter(index,index,inputString);
            String substring2 = extendAroundCenter(index,index +1, inputString);
            if(substring1.length() > substring2.length() && maxSubString.length() < substring1.length())
            {
                maxSubString = substring1;
            }
            else if(substring1.length() < substring2.length() && maxSubString.length() < substring2.length() )
            {
                maxSubString = substring2;
            }
        }
        return maxSubString;
    }
    public  String extendAroundCenter(int leftIndex,int rightIndex, String inputString)
    {
        int left = leftIndex;
        int right = rightIndex;
        while(left >= 0 && right < inputString.length() && inputString.charAt(left) == inputString.charAt(right))
        {
            left--;
            right++;
        }
        //int palindromicIndex = right - left +1;
        return inputString.substring(left+1,right);
    }

}
