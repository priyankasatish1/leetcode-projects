public class ContainerWithMaxWater {
    public int maxArea(int[] heights)
    {
        int left = 0;
        int right = heights.length-1;
        int area;
        int maxArea = Integer.MIN_VALUE;

        while(left < right)
        {
            area = (right - left) * Math.min(heights[left],heights[right]);
            if (area > maxArea)
            {
                maxArea = area;
            }
            if(heights[left] < heights[right])
            {
                left++;
            }
            else
            {
                right--;
            }
        }
        return maxArea;
    }
}
