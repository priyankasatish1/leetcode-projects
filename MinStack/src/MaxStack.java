import java.util.Stack;

public class MaxStack {
    Stack<Integer> stack;
    Stack <Pair> maxStack;
    Stack<Integer> maxBuffer;
    public MaxStack()
    {
        stack = new Stack<>();
        maxStack = new Stack<>();
        maxBuffer = new Stack<>();

    }
    public void push(int number)
    {
        stack.push(number);
        if(maxStack.isEmpty())
        {
            maxStack.push(new Pair(number,1));
        }
        else if(maxStack.peek().key == number)
        {
          int newFrequency = maxStack.pop().value +1;
          maxStack.push(new Pair(number,newFrequency));
        }
        else if(stack.peek() > maxStack.peek().key)
        {
            maxStack.push(new Pair(number,1));
        }

    }
    public int pop()
    {
        int newElement = stack.pop();
        if(newElement == maxStack.peek().key)
        {
            int newFrequency = maxStack.peek().value-1;
            if(newFrequency == 0)
            {
                maxStack.pop();
            }
            else
            {
                maxStack.pop();
                maxStack.push(new Pair(newElement,newFrequency));
            }
        }
        return newElement;
    }
    public int getMax()
    {
        int maxValue = maxStack.peek().key;
        return maxValue;
    }
    public int top()
    {
        int stackTop = stack.peek();
        return stackTop;
    }
    public int popMax()
    {
       int maxItem = maxStack.peek().key;
       while(maxItem != top())
       {
           maxBuffer.push(pop());
       }
       pop();
       while(!maxBuffer.empty())
       {
           push(maxBuffer.pop());
       }
       return maxItem;

    }
}
