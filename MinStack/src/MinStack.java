import java.util.Stack;

public class MinStack {
    Stack<Integer> stack;
    Stack<Pair> minStack;
    public MinStack()
    {
        stack = new Stack<>();
        minStack = new Stack<>();
    }
    public void push(int number)
    {
        stack.push(number);
        if(minStack.empty())
        {
            minStack.push(new Pair(number,1));
        }
        else if(stack.peek() == minStack.peek().key)
        {
            int newFrequency = minStack.pop().value +1;
            minStack.push(new Pair(number,newFrequency));
        }
        else if(stack.peek() < minStack.peek().key)
        {
            minStack.push(new Pair(number,1));
        }
    }
    public int pop()
    {
        int stackPop = stack.pop();
        if(stackPop == minStack.peek().key)
        {
            int newMinFrequency = minStack.peek().value - 1;
            if(newMinFrequency == 0)
            {
                minStack.pop();
            }
            else
            {
                minStack.pop();
                minStack.push(new Pair(stackPop,newMinFrequency));
            }
        }
        return stackPop;
    }
    public int getMin()
    {
        int currMinStack = minStack.peek().key;
        return currMinStack;
    }
    public  int getTop()
    {
        int currTop = stack.peek();
        return currTop;
    }
}
