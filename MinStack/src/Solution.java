public class Solution {
    public static void main(String[] args)
    {
        MinStack ms = new MinStack();
        ms.push(-2);
        ms.push(0);
        ms.push(-3);
        int min = ms.getMin();
        System.out.println(min);
        int pop = ms.pop();
        System.out.println(pop);
        int top = ms.getTop();
        System.out.println(top);
        int getMin = ms.getMin();
        System.out.println(getMin);

        //int getMin = ms.getMin();
        //System.out.println(getMin);
        //int pop1 = ms.pop();
        //System.out.println(pop1);
        //int min2 = ms.getMin();
        //System.out.println(min2);
        //int pop3 = ms.pop();
        //System.out.println(pop3);
        //int min4 = ms.getMin();
        //System.out.println(min4);

    }
}
