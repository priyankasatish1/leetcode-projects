public class ProductOfArrays {
    public static int[] productExceptSelf(int[] nums){
        int left = 1;
        int right = 1;
        int[] result = new int[nums.length];
        for(int index = 0; index < nums.length; index++)
        {
            result[index]=1;
        }
        for(int index = 0; index < nums.length; index++)
        {
            result[index] = left;
            left = left * nums[index];

        }
        for(int index = nums.length-1; index >= 0; index --)
        {
            result[index] = right * result[index];
            right = right * nums[index];
        }
        return result;
    }
    public static void main(String [] args)
    {
        int[] nums = {0,1,0,1};
        int[] result = productExceptSelf(nums);
        for(int i = 0; i < nums.length; i++)
        {
            System.out.println(result[i]);
        }
    }

}
