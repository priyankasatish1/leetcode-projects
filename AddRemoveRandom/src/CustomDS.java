import java.util.*;

public class CustomDS {
    public  HashMap<Integer, Integer> map;
    public  List<Integer> list;

    public CustomDS()
    {
        map = new HashMap<>();
        list = new ArrayList<>();
    }
    public  boolean add(Integer value)
    {
        if (!map.containsKey(value))
        {
            int size = list.size();
            list.add(value);
            map.put(value, size);
            return true;
        }

        return false;
    }
    public boolean remove(Integer value)
    {
        if (map.containsKey(value))
        {
            int index = map.get(value);
            int lastIndex = list.size() - 1;
            Collections.swap(list, lastIndex, index);
            map.put(list.get(index),index);
            list.remove(list.size() - 1);
            map.remove(value);
            return true;
        }
        return false;
    }
    public int randomRemove()
    {
        Random rand = new Random();
        int indexToRemove = rand.nextInt(list.size());
        int element = list.get(indexToRemove);
        //remove(element);
        return element;
        //return list.get(rand.nextInt(list.size()));
    }

}
