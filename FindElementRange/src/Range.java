

public class Range {
    public static int[] searchRange(int[] nums, int target) {
        int startIndex = binarySearchStart(nums,0,nums.length-1,target);
        int endIndex = binarySearchEnd(nums,0,nums.length-1,target);
        int[] res = new int[2];
        res[0] = startIndex;
        res[1] = endIndex;
        return res;
    }
    public static int binarySearchStart(int[] nums, int lo, int hi, int target)
    {
        if(lo > hi)
        {
            return -1;
        }
        if(nums.length == 0)
        {
            return -1;
        }
        if(lo == hi)
        {
            if(nums[lo]==target)
            {
                return lo;
            }
            return -1;
        }
        int mid = (lo + hi)/2;
        if((mid==0||target > nums[mid-1]) && nums[mid] == target)
        {
            return mid;
        }
        else if(target > nums[mid])
        {
            return binarySearchStart(nums,mid+1,hi,target);
        }
        else
        {
            return binarySearchStart(nums,lo,mid-1,target);
        }
    }
    public static int binarySearchEnd(int[] nums, int lo, int hi, int target)
    {
        int ans = 0;
        if(lo > hi)
        {
            return  -1;
        }
        if(nums.length == 0)
        {
            return -1;
        }
        if(lo == hi)
        {
            if(nums[lo]==target)
            {
                return lo;
            }
            return -1;
        }
        int mid = (lo + hi)/2;
        if((mid==nums.length-1||target < nums[mid+1]) && nums[mid] == target)
        {
            return mid;
        }
        else if(target < nums[mid])
        {
            return  binarySearchEnd(nums,lo,mid-1,target);
        }
        else
        {
            return  binarySearchEnd(nums,mid+1,hi,target);
        }
    }
    public static void main(String[] args)
    {
        int[] res = new int[2];
        int[] input = {2,2};
        res = searchRange(input,3);

    }
}
