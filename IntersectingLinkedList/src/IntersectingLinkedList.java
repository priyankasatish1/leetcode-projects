public class IntersectingLinkedList {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB){
        int length1 = getLength(headA);
        int length2 = getLength(headB);
        ListNode currentNode1 = headA;
        ListNode currentNode2 = headB;
        if(length1 > length2)
        {
            currentNode1 = equalizeList(headA,length1-length2);
        }
        else
        {
            currentNode2 = equalizeList(headB,length2-length1);
        }
        while(currentNode1!=null)
        {
            if(currentNode1 == currentNode2)
            {
                return currentNode1;
            }
            currentNode1 = currentNode1.link;
            currentNode2 = currentNode2.link;
        }
        return null;
    }
    public  ListNode equalizeList(ListNode node, int difference)
    {
        while(difference!=0)
        {
            node = node.link;
            difference--;
        }
        return node;
    }
    public int getLength(ListNode head)
    {
        ListNode tempNode = head;
        int length =0;
        while(tempNode!=null)
        {
            tempNode = tempNode.link;
            length++;
        }
        return length;
    }
}
