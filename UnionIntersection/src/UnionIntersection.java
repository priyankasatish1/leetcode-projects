import java.util.*;

public class UnionIntersection
{
    public static List<Integer> union(List<Integer> a, List<Integer> b)
    {
        Iterator<Integer> aIterator = a.iterator();
        Iterator<Integer> bIterator = b.iterator();
        List<Integer> c = new LinkedList<>();
        Integer num1 = aIterator.hasNext()?aIterator.next():null;
        Integer num2 = bIterator.hasNext()?bIterator.next():null;
        while (num1!=null && num2!=null) {
            if (num1 < num2) {
                c.add(num1);
                num1 = aIterator.hasNext() ? aIterator.next() : null;
            } else {
                c.add(num2);
                num2 = bIterator.hasNext() ? bIterator.next() : null;
            }
        }
        while (num1 != null)
        {
            c.add(num1);
            num1 = aIterator.hasNext()?aIterator.next():null;

        }
        while(num2!=null)
        {
            c.add(num2);
            num2 = bIterator.hasNext()?bIterator.next():null;
        }

        return c;
    }
    public static List<Integer> intersect(List<Integer> l1, List<Integer> l2)
    {
        List<Integer> l3 = new LinkedList<>();
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        Iterator<Integer> it1 = l1.iterator();
        Iterator<Integer> it2 = l2.iterator();

        while(it1.hasNext())
        {
            set1.add(it1.next());
        }

        while(it2.hasNext())
        {
            set2.add(it2.next());
        }

        set1.retainAll(set2);
        Iterator<Integer> it = set1.iterator();
        while (it.hasNext())
        {
            l3.add(it.next());
        }
        return l3;

    }
    public static void main(String args[])
    {
        List<Integer> l1 = new LinkedList<>();
        l1.add(1);
        l1.add(3);
        l1.add(5);
        List<Integer> l2 = new LinkedList<>();
        l2.add(1);
        l2.add(4);
        l2.add(6);
        List<Integer> l3 = union(l1,l2);
        List<Integer> l4 = intersect(l1,l2);

    }
}
