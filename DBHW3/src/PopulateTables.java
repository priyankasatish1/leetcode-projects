import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PopulateTables {

    public void populateMovies(String filename) throws SQLException
    {
        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM movies");
        String genresPath = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(genresPath);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line=bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement = null;

            String ID;
            String title;
            String imdbID;
            String spanishTitle;
            String imdbPictureURL;
            int year;
            String rtID;
            float rtAllCriticsRating;
            float rtAllCriticsNumReviews;
            float rtAllCriticsNumFresh;
            float rtAllCriticsNumRotten;
            float rtAllCriticsScore;
            float rtTopCriticsRating;
            float rtTopCriticsNumReviews;
            float rtTopCriticsNumFresh;
            float rtTopCriticsNumRotten;
            float rtTopCriticsScore;
            float rtAudienceRating;
            float rtAudienceNumRatings;
            float rtAudienceScore;
            String rtPictureURL;

            while (line!=null)
            {
                String queryRow[] = line.split("\t");
                if(queryRow.length>0) {
                    ID = queryRow[0];
                    title = queryRow[1];
                    imdbID = queryRow[2];
                    spanishTitle = queryRow[3];
                    imdbPictureURL = queryRow[4];
                    year = Integer.parseInt(queryRow[5]);
                    rtID = queryRow[6];
                    rtAllCriticsRating = queryRow[7].equals("\\N") ? 0 : Float.parseFloat(queryRow[7]);
                    rtAllCriticsNumReviews = queryRow[8].equals("\\N") ? 0 : Float.parseFloat(queryRow[8]);
                    rtAllCriticsNumFresh = queryRow[9].equals("\\N") ? 0 : Float.parseFloat(queryRow[9]);
                    rtAllCriticsNumRotten = queryRow[10].equals("\\N") ? 0 : Float.parseFloat(queryRow[10]);
                    rtAllCriticsScore = queryRow[11].equals("\\N") ? 0 : Float.parseFloat(queryRow[11]);
                    rtTopCriticsRating = queryRow[12].equals("\\N") ? 0 : Float.parseFloat(queryRow[12]);
                    rtTopCriticsNumReviews = queryRow[13].equals("\\N") ? 0 : Float.parseFloat(queryRow[13]);
                    rtTopCriticsNumFresh = queryRow[14].equals("\\N") ? 0 : Float.parseFloat(queryRow[14]);
                    rtTopCriticsNumRotten = queryRow[15].equals("\\N") ? 0 : Float.parseFloat(queryRow[15]);
                    rtTopCriticsScore = queryRow[16].equals("\\N") ? 0 : Float.parseFloat(queryRow[16]);
                    rtAudienceRating = queryRow[17].equals("\\N") ? 0 : Float.parseFloat(queryRow[17]);
                    rtAudienceNumRatings = queryRow[18].equals("\\N") ? 0 : Float.parseFloat(queryRow[18]);
                    rtAudienceScore = queryRow[19].equals("\\N") ? 0 : Float.parseFloat(queryRow[19]);
                    rtPictureURL = queryRow[20];

                    sqlStatement = "INSERT INTO MOVIES " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                    try {
                        preparedStatement = DBConnect.dbConnection.prepareStatement(sqlStatement);
                        preparedStatement.setString(1, ID);
                        preparedStatement.setString(2, title);
                        preparedStatement.setString(3, imdbID);
                        preparedStatement.setString(4, spanishTitle);
                        preparedStatement.setString(5, imdbPictureURL);
                        preparedStatement.setInt(6, year);
                        preparedStatement.setString(7, rtID);
                        preparedStatement.setDouble(8, rtAllCriticsRating);
                        preparedStatement.setDouble(9, rtAllCriticsNumReviews);
                        preparedStatement.setDouble(10, rtAllCriticsNumFresh);
                        preparedStatement.setDouble(11, rtAllCriticsNumRotten);
                        preparedStatement.setDouble(12, rtAllCriticsScore);
                        preparedStatement.setDouble(13, rtTopCriticsRating);
                        preparedStatement.setDouble(14, rtTopCriticsNumReviews);
                        preparedStatement.setDouble(15, rtTopCriticsNumFresh);
                        preparedStatement.setDouble(16, rtTopCriticsNumRotten);
                        preparedStatement.setDouble(17, rtTopCriticsScore);
                        preparedStatement.setDouble(18, rtAudienceRating);
                        preparedStatement.setDouble(19, rtAudienceNumRatings);
                        preparedStatement.setDouble(20, rtAudienceScore);
                        preparedStatement.setString(21, rtPictureURL);
                        preparedStatement.executeUpdate();
                    }
                    catch (SQLException e)
                    {
                        System.out.println("FAILED TO PREPARE STATEMENT FOR TABLE MOVIES");
                        e.printStackTrace();
                        return;
                    }
                    finally
                    {
                        try
                        {
                            preparedStatement.close();
                        }
                        catch (SQLException ex)
                        {
                            //Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                else
                    System.out.println("THERE IS A COLUMN MISMATCH IN MOVIES TABLE");
                line=bufferedReader.readLine();
            }

        }
        catch (FileNotFoundException e)
        {
            System.out.println("Please check the input. MOVIES.dat file was not found");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //Make sure to close the buffered reader and the file after the preparedStatement is closed.
        finally
        {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            }
            catch (IOException ex)
            {

                ex.printStackTrace();
            }
        }

        System.out.println("DATA SUCCESSFULLY INSERTED INTO MOVIES");
        System.out.println("*************************************************");
    }

    /*public void populateMovieDirectors(String filename) throws SQLException
    {
        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM movie_directors");
        String genresPath = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(genresPath);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line=bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement = null;
            String movieID = null;
            String directorID;
            String directorName;
            while (line!=null){
                String row[] = line.split("\\t");

                if(row.length>0)
                    movieID=row[0];
                directorID = row.length>1 ? row[1] : "";
                directorName = row.length>2 ? row[2] : "";
                sqlStatement = "INSERT INTO movie_directors " + "VALUES (?, ?, ?)";

                try {
                    preparedStatement=DBConnect.dbConnection.prepareStatement(sqlStatement);
                    preparedStatement.setString(1, movieID);
                    preparedStatement.setString(2, directorID);
                    preparedStatement.setString(3, directorName);
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("FAILED TO PREPARE STATEMENT FOR TABLE movie_directors TABLE");
                    e.printStackTrace();
                    return;
                }finally{
                    if(preparedStatement!=null){
                        try {
                            preparedStatement.close();
                        } catch (SQLException e) {
                            Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }

                line=bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("Please check the input. movie_directors file not found");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            } catch (IOException ex) {

                ex.printStackTrace();
            }
        }
        System.out.println("DATA SUCCESSFULLY INSERTED INTO MOVIE_DIRECTORS");
        System.out.println("*************************************************");
    }*/

    public void populateMovieGenres(String filename) throws SQLException
    {
        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM movie_genres");
        String path = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(path);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line=bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement = null;

            String movieID;
            String genre;


            while (line!=null)
            {
                String queryRow[] = line.split("\t");
                if(queryRow.length>0) {
                    movieID = queryRow[0];
                    genre = queryRow[1];


                    sqlStatement = "INSERT INTO MOVIE_GENRES " + "VALUES (?, ?)";

                    try {
                        preparedStatement = DBConnect.dbConnection.prepareStatement(sqlStatement);
                        preparedStatement.setString(1, movieID);
                        preparedStatement.setString(2, genre);

                        preparedStatement.executeUpdate();
                    }
                    catch (SQLException e)
                    {
                        System.out.println("FAILED TO PREPARE STATEMENT FOR TABLE MOVIE GENRES");
                        e.printStackTrace();
                        return;
                    }
                    finally
                    {
                        try
                        {
                            preparedStatement.close();
                        }
                        catch (SQLException ex)
                        {
                            //Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                else
                    System.out.println("THERE IS A COLUMN MISMATCH IN MOVIES GENRE TABLE");
                line=bufferedReader.readLine();
            }

        }
        catch (FileNotFoundException e)
        {
            System.out.println("Please check the input. MOVIE_GENRES.dat file was not found");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //Make sure to close the buffered reader and the file after the preparedStatement is closed.
        finally
        {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            }
            catch (IOException ex)
            {

                ex.printStackTrace();
            }
        }

        System.out.println("DATA SUCCESSFULLY INSERTED INTO MOVIE_GENRES");
        System.out.println("*************************************************");
    }

    /*public void populateMovieActors(String filename) throws SQLException
    {
        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM movie_actors");

        String path = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(path);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line= bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement = null;
            String movieID = null;
            String actorID;
            String actorName;
            int ranking;
            while (line!=null){
                String row[] = line.split("\\t");

                if(row.length>0) {
                    movieID = row[0];
                }
                actorID = row.length>1 ? row[1] : "";
                actorName = row.length>2 ? row[2] : "";
                ranking = row.length>3 ? Integer.parseInt(row[3]) : 0;
                sqlStatement = "INSERT INTO movie_actors " + "VALUES (?, ?, ?, ?)";

                try {
                    preparedStatement=DBConnect.dbConnection.prepareStatement(sqlStatement);
                    preparedStatement.setString(1, movieID);
                    preparedStatement.setString(2, actorID);
                    preparedStatement.setString(3, actorName);
                    preparedStatement.setInt(4, ranking);
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("FAILED TO PREPARE STATEMENT FOR movie_actors TABLE");
                    e.printStackTrace();
                    return;
                }finally{
                    if(preparedStatement!=null){
                        try {
                            preparedStatement.close();
                        } catch (SQLException e) {
                            Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                line=bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("Please check the input. movie_actors file was not found");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            } catch (IOException ex) {

                ex.printStackTrace();
            }
        }

        System.out.println("DATA SUCCESSFULLY INSERTED INTO MOVIE_ACTORS");
        System.out.println("*************************************************");
    }*/

    public void populateMovieCountries(String filename) throws SQLException
    {

        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM movie_countries");
        String path = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(path);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line= bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement = null;
            String movieID;
            String country;


            while (line!=null){
                String row[] = line.split("\\t");
                if(row.length>0)
                {
                    movieID=row[0];
                    country = row.length>1 ? row[1]:"";
                    sqlStatement = "INSERT INTO MOVIE_COUNTRIES " + "VALUES (?, ?)";

                    try {
                        preparedStatement=DBConnect.dbConnection.prepareStatement(sqlStatement);
                        preparedStatement.setString(1, movieID);
                        preparedStatement.setString(2, country);
                        preparedStatement.executeUpdate();
                    } catch (SQLException e) {
                        System.out.println("FAILED TO PREPARE STATEMENT FOR movie_countries TABLE");
                        e.printStackTrace();
                        return;
                    }finally{
                        if(preparedStatement!=null){
                            try {
                                preparedStatement.close();
                            } catch (SQLException e) {
                                Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, e);
                            }
                        }
                    }
                }
                else{
                    System.out.println("MOVIE ID AS NULL VALUE IS NOT ALLOWED");

                }


                line=bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("Please check the input. movie_countries file was not found");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            } catch (IOException ex) {

                ex.printStackTrace();
            }
        }

        System.out.println("DATA SUCCESSFULLY INSERTED INTO MOVIE_COUNTRIES");
        System.out.println("*************************************************");

    }

    public void populateMovieLocations(String filename) throws SQLException
    {

        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM movie_locations");

        String path = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(path);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line= bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement = null;
            String movieID;
            String location1;
            String location2;
            String location3;
            String location4;


            while (line!=null){
                String row[] = line.split("\\t");
                movieID = row.length>0 ? row[0]:"";
                location1 = row.length>1 ? row[1]:"";
                location2 = row.length>2 ? row[2]:"";
                location3 = row.length>3 ? row[3]:"";
                location4 = row.length>4 ? row[4]:"";

                sqlStatement = "INSERT INTO MOVIE_LOCATIONS (movieID,location1,location2,location3,location4) " + "VALUES (?, ?, ?, ?, ?)";

                try {
                    preparedStatement= DBConnect.dbConnection.prepareStatement(sqlStatement);
                    preparedStatement.setString(1, movieID);
                    preparedStatement.setString(2, location1);
                    preparedStatement.setString(3, location2);
                    preparedStatement.setString(4, location3);
                    preparedStatement.setString(5, location4);
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("FAILED TO PREPARE STATEMENT FOR movie_locations TABLE");
                    e.printStackTrace();
                    return;
                }finally{
                    if(preparedStatement!=null){
                        try {
                            preparedStatement.close();
                        } catch (SQLException e) {
                            Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }

                line=bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("Please check the input. movie_locations file was not found");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            } catch (IOException ex) {

                ex.printStackTrace();
            }
        }

        System.out.println("DATA SUCCESSFULLY INSERTED INTO MOVIE_LOCATIONS");
        System.out.println("*************************************************");
    }

    /*public void populateTags(String filename) throws SQLException
    {

        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM tags");

        String path = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(path);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line= bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement = null;
            String tagID;
            String tagText;

            while (line!=null){
                String row[] = line.split("\\t");
                tagID = row.length>0 ? row[0]:"";
                tagText = row.length>1 ? row[1]:"";

                sqlStatement = "INSERT INTO tags "+ "VALUES (?, ?)";
                try {
                    preparedStatement=DBConnect.dbConnection.prepareStatement(sqlStatement);
                    preparedStatement.setString(1, tagID);
                    preparedStatement.setString(2, tagText);
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("FAILED TO PREPARE STATEMENT FOR tags TABLE");
                    e.printStackTrace();
                    return;
                }finally{
                    if(preparedStatement!=null){
                        try {
                            preparedStatement.close();
                        } catch (SQLException e) {
                            Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                line=bufferedReader.readLine();
            }

        }
        catch (FileNotFoundException e)
        {
            System.out.println("Please check the input. tags file was not found");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally {
            try
            {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            }
            catch (IOException ex)
            {

                ex.printStackTrace();
            }
        }
        System.out.println("DATA SUCCESSFULLY INSERTED INTO TAGS");
        System.out.println("*************************************************");
    }*/

    /*public void populateMovieTags(String filename) throws SQLException
    {
        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM movie_tags");

        String path = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(path);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line= bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement =null;
            String movieID;
            String tagID;
            int tagWeight;

            while (line!=null){
                String row[] = line.split("\\t");
                movieID = row.length>0 ? row[0]:"";
                tagID = row.length>1 ? row[1]:"";
                tagWeight = row.length>2 ? Integer.parseInt(row[2]) : 0;

                sqlStatement = "INSERT INTO movie_tags " + "VALUES (?, ?, ?)";
                try {
                    preparedStatement=DBConnect.dbConnection.prepareStatement(sqlStatement);
                    preparedStatement.setString(1, movieID);
                    preparedStatement.setString(2, tagID);
                    preparedStatement.setInt(3, tagWeight);
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("FAILED TO PREPARE STATEMENT FOR movie_tags TABLE");
                    e.printStackTrace();
                    return;
                }finally{
                    if(preparedStatement!=null){
                        try {
                            preparedStatement.close();
                        } catch (SQLException e) {
                            Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                line=bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("Please check the input. movie_tags file was not found");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            } catch (IOException ex) {

                ex.printStackTrace();
            }
        }

        System.out.println("DATA SUCCESSFULLY INSERTED INTO MOVIE_TAGS");
        System.out.println("*************************************************");
    }*/

    /*public void populateUserRatedMovies(String filename) throws SQLException
    {
        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM user_ratedmovies");
        String path = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(path);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line= bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement =null;
            String userID;
            String movieID;
            String rating ;
            int date_day;
            int date_month;
            int date_year;
            int date_hour;
            int date_minute;
            int date_second;

            while (line!=null){
                String row[] = line.split("\\t");
                userID = row.length>0 ? row[0]:"";
                movieID = row.length>1 ? row[1]:"";
                rating = row.length>2 ? row[2]:"";
                date_day = row.length>3 ? Integer.parseInt(row[3]) : 0;
                date_month = row.length>4 ? Integer.parseInt(row[4]) : 0;
                date_year = row.length>5 ? Integer.parseInt(row[5]) : 0;
                date_hour = row.length>6 ? Integer.parseInt(row[6]) : 0;
                date_minute = row.length>7 ? Integer.parseInt(row[7]) : 0;
                date_second = row.length>8 ? Integer.parseInt(row[8]) : 0;

                sqlStatement = "INSERT INTO user_ratedmovies " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                try {
                    preparedStatement=DBConnect.dbConnection.prepareStatement(sqlStatement);
                    preparedStatement.setString(1, userID);
                    preparedStatement.setString(2, movieID);
                    preparedStatement.setString(3, rating);
                    preparedStatement.setInt(4, date_day);
                    preparedStatement.setInt(5, date_month);
                    preparedStatement.setInt(6, date_year);
                    preparedStatement.setInt(7, date_hour);
                    preparedStatement.setInt(8, date_minute);
                    preparedStatement.setInt(9, date_second);
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("FAILED TO PREPARE STATEMENT FOR user_ratedmovies TABLE");
                    e.printStackTrace();
                    return;
                }finally{
                    if(preparedStatement!=null){
                        try {
                            preparedStatement.close();
                        } catch (SQLException e) {
                            Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                line=bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("Please check the input. user_ratedmovies file was not found");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            } catch (IOException ex) {

                ex.printStackTrace();
            }
        }

        System.out.println("DATA SUCCESSFULLY INSERTED INTO USER_RATEDMOVIES");
        System.out.println("*************************************************");
    }*/

    /*public void populateUserTaggedMovies(String filename) throws SQLException
    {
        Statement statement = DBConnect.dbConnection.createStatement();
        statement.executeUpdate("DELETE FROM user_taggedmovies");
        String path = DBConnect.path+filename;
        FileReader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(path);
            bufferedReader  = new BufferedReader(reader);
            bufferedReader.readLine();
            String line= bufferedReader.readLine();
            String sqlStatement;
            PreparedStatement preparedStatement =null;
            String userID;
            String movieID;
            String tagID ;
            int date_day;
            int date_month;
            int date_year;
            int date_hour;
            int date_minute;
            int date_second;

            while (line!=null){
                String row[] = line.split("\\t");
                userID = row.length>0 ? row[0]:"";
                movieID = row.length>1 ? row[1]:"";
                tagID = row.length>2 ? row[2]:"";
                date_day = row.length>3 ? Integer.parseInt(row[3]) : 0;
                date_month = row.length>4 ? Integer.parseInt(row[4]) : 0;
                date_year = row.length>5 ? Integer.parseInt(row[5]) : 0;
                date_hour = row.length>6 ? Integer.parseInt(row[6]) : 0;
                date_minute = row.length>7 ? Integer.parseInt(row[7]) : 0;
                date_second = row.length>8 ? Integer.parseInt(row[8]) : 0;

                sqlStatement = "INSERT INTO user_taggedmovies " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                try {
                    preparedStatement=DBConnect.dbConnection.prepareStatement(sqlStatement);
                    preparedStatement.setString(1, userID);
                    preparedStatement.setString(2, movieID);
                    preparedStatement.setString(3, tagID);
                    preparedStatement.setInt(4, date_day);
                    preparedStatement.setInt(5, date_month);
                    preparedStatement.setInt(6, date_year);
                    preparedStatement.setInt(7, date_hour);
                    preparedStatement.setInt(8, date_minute);
                    preparedStatement.setInt(9, date_second);
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("FAILED TO PREPARE STATEMENT FOR user_taggedmovies TABLE");
                    e.printStackTrace();
                    return;
                }finally{
                    if(preparedStatement!=null){
                        try {
                            preparedStatement.close();
                        } catch (SQLException e) {
                            Logger.getLogger(PopulateTables.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                }
                line=bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("Please check the input. user_taggedmovies file was not found");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();

                if (reader != null)
                    reader.close();
            } catch (IOException ex) {

                ex.printStackTrace();
            }
        }

        System.out.println("DATA SUCCESSFULLY INSERTED INTO USER_TAGGEDMOVIES");
        System.out.println("*************************************************");
    }*/

}
