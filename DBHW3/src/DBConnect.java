import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {

    public static final String DRIVER  = "com.mysql.jdbc.Driver";
    public static final String CONNECTION = "jdbc:mysql://localhost:3306/imdb_psatish";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "priyanka";
    public static Connection dbConnection=null;
    public static String path = "/Users/priyankasatish/workspace/database/hw3data/hetrec2011-movielens-2k-v2/";

    public static Connection getDBConnection(){
        try {
            Class.forName(DRIVER);
            dbConnection= DriverManager.getConnection(CONNECTION,USERNAME,PASSWORD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dbConnection;
    }
    public static void main(String[] args) {
        dbConnection=getDBConnection();
        PopulateTables pop = new PopulateTables();

        try {
            pop.populateMovies("movies.dat");
            pop.populateMovieGenres("movie_genres.dat");
            //pop.populateMovieDirectors("movie_directors.dat");
            //pop.populateMovieActors("movie_actors.dat");
            pop.populateMovieCountries("movie_countries.dat");
            pop.populateMovieLocations("movie_locations.dat");
            //pop.populateTags("tags.dat");
            //pop.populateMovieTags("movie_tags.dat");
            //pop.populateUserRatedMovies("user_ratedmovies.dat");
            //pop.populateUserTaggedMovies("user_taggedmovies.dat");
       }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        closeConnection();

    }
    public static void closeConnection() {
        try {
            dbConnection.close();
        } catch (SQLException e) {
            System.err.println("Cannot close connection: " + e.getMessage());
        }
    }
}
